import "./Registration.css";
import { useState } from "react";
const Registration = () => {
  const [inputValues, setInputValues] = useState({
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    dateOfBirth: "",
    gender: "",
    accepted: false,
  });
  const emailChanged = event => {
    setInputValues({
      ...inputValues,
      email: event.target.value,
    });
  };
  const passwordChanged = event => {
    setInputValues({
      ...inputValues,
      password: event.target.value,
    });
  };
  const firstNameChanged = event => {
    setInputValues({
      ...inputValues,
      firstName: event.target.value,
    });
  };
  const lastNameChanged = event => {
    setInputValues({
      ...inputValues,
      lastName: event.target.value,
    });
  };
  const dateOfBirthChanged = event => {
    setInputValues({
      ...inputValues,
      dateOfBirth: event.target.value,
    });
  };
  const genderChanged = event => {
    setInputValues({
      ...inputValues,
      gender: event.target.value,
    });
  };
  const acceptedChanged = event => {
    setInputValues({
      ...inputValues,
      accepted: event.target.value,
    });
  };
  const resetInfo = () => {
    setInputValues({
      email: "",
      password: "",
      firstName: "",
      lastName: "",
      dateOfBirth: "",
      gender: "",
      accepted: false,
    });
  };
  const submitInfo = () => {
    if (
      inputValues.email &&
      inputValues.password &&
      inputValues.firstName &&
      inputValues.lastName &&
      inputValues.dateOfBirth &&
      inputValues.gender &&
      inputValues.accepted
    )
      alert(
        `email: ${inputValues.email}\npassword:  ${inputValues.password}
First Name: ${inputValues.firstName}\nLast Name: ${inputValues.lastName}
Date of Birth: ${inputValues.dateOfBirth}\nGender: ${inputValues.gender}
Accepted Privacy Policy: Accepted`
      );
    else alert("Fill in all the fields");
  };

  return (
    <div class="register__form-wrapper">
      <h1>Registration</h1>
      <form class="register__form">
        <label for="Email" class="email__label">
          Email adress:
        </label>
        <input
          type="email"
          placeholder="Email"
          id="Email"
          className="register__form--input"
          value={inputValues.email}
          onChange={emailChanged}
          required
        />
        <label for="password" class="password__label">
          Password:
        </label>
        <input
          type="password"
          Placeholder="Password"
          id="password"
          className="register__form--input"
          value={inputValues.password}
          onChange={passwordChanged}
          required
        />
        <label for="firstname" class="first-name__label">
          First name:
        </label>
        <input
          type="text"
          placeholder="First Name"
          id="firstname"
          className="register__form--input"
          value={inputValues.firstName}
          onChange={firstNameChanged}
          required
        />
        <label for="lastname" class="last-name__label">
          Last name:
        </label>
        <input
          type="text"
          placeholder="Last name"
          id="lastname"
          className="register__form--input"
          value={inputValues.lastName}
          onChange={lastNameChanged}
          required
        />
        <label for="Date-Birth" class="date-birth__label">
          Date of Birth:
        </label>
        <input
          type="date"
          id="Date-Birth"
          className="register__form--input"
          value={inputValues.dateOfBirth}
          onChange={dateOfBirthChanged}
        />
        <div clasName="gender__wrapper">
          <p>Please select your gender:</p>
          <input
            type="radio"
            id="male"
            name="gender"
            value="male"
            onChange={genderChanged}
          />
          <label for="male">Male</label>
          <input
            type="radio"
            id="female"
            name="gender"
            value="female"
            onChange={genderChanged}
          />
          <label for="female">Female</label>
          <input
            type="radio"
            id="other"
            name="gender"
            value="other"
            onChange={genderChanged}
          />
          <label for="other">Other</label>
        </div>
        <div className="checkbox__wrapper">
          <input
            type="checkbox"
            className="terms-privacy__checkbox"
            id="checkbox"
            checked={inputValues.accepted}
            onChange={acceptedChanged}
          />
          <label for="checkbox">I read and accepted privacy policy</label>
        </div>
        <button className="submit__button" onClick={submitInfo}>
          Submit
        </button>
        <button className="reset__button" onClick={resetInfo}>
          Reset
        </button>{" "}
      </form>
    </div>
  );
};
export default Registration;
